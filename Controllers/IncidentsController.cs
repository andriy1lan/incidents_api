using System.Security.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using incidents.Models;

namespace incidents.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IncidentsController : ControllerBase
    {
        private IncidentContext context=new IncidentContext();

        // GET api/incidents
        [HttpGet]
        public ActionResult<IList<Incident>> GetIncidents()
        {
            var incidents = context.Incidents.ToList();
            if (incidents.Count!= 0)
            {
                return Ok(incidents);
            }
            else { return NotFound(); }
        }

        // GET api/incidents/5
        [HttpGet("{id}")]
        public ActionResult<Incident> GetIncident(Guid id)
        {
            var incident = context.Incidents.Where(i=>i.Id.Equals(id)).FirstOrDefault();
            if (incident != null)
            {
                var account=context.Accounts.Where(a=>a.Name.Equals(incident.AccountName)).FirstOrDefault();
                if (account!= null)
                {
                var contact=context.Contacts.Where(a=>a.Email.Equals(account.ContactEmail)).FirstOrDefault();
                account.Contact=contact;
                } 
                incident.Account=account;
                return Ok(incident);
            }
            else { return NotFound(); }
        }

        [HttpPost]
        public ActionResult<Incident> PostIncident([FromBody]IncidentDTO incidentDTO)
        {
            var account=context.Accounts.Where(a=>a.Name.Equals(incidentDTO.AccountName)).FirstOrDefault();
            if (account==null) return NotFound();
            var contact=context.Contacts.Where(a=>a.Email.Equals(incidentDTO.Email)).FirstOrDefault();
            
            if (contact==null) {
                var newContact=new Contact {
                FirstName=incidentDTO.FirstName, LastName=incidentDTO.LastName,
                Email=incidentDTO.Email
                };
                context.Contacts.Add(newContact);
                account.Contact=newContact; //Link Account to new Contact
            }

            var incident=new Incident {Account=account, Description=incidentDTO.Description};
            context.Incidents.Add(incident);
            context.SaveChanges();
            //return StatusCode(201,incident);
            return CreatedAtAction(nameof(GetIncident), new { id = incident.Id }, incident);
        }

    }
}
using System.ComponentModel.Design.Serialization;
using System.Diagnostics.Contracts;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using incidents.Models;

namespace incidents.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private IncidentContext context=new IncidentContext();

        // GET api/contacts
        [HttpGet]
        public ActionResult<IList<Contact>> GetContacts()
        {
            var contacts = context.Contacts.ToList();
            if (contacts.Count!= 0)
            {
                return Ok(contacts);
            }
            else { return NotFound(); }
        }

        // GET api/contacts/5
        [HttpGet("{email}")]
        public ActionResult<Contact> GetContact(string email)
        {
            var contact = context.Contacts.Where(a=>a.Email.Equals(email));
            if (contact!= null)
            {
                return Ok(contact);
            }
            else { return NotFound(); }
        }

        [HttpPost]
        public ActionResult<Contact> PostContact([FromBody]Contact contact)
        {
            context.Contacts.Add(contact);
            context.SaveChanges();
            return StatusCode(201);
        }
    }
}
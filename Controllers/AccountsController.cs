using System.ComponentModel.Design.Serialization;
using System.Diagnostics.Contracts;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using incidents.Models;

namespace incidents.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
         private IncidentContext context=new IncidentContext();

        // GET api/accounts
        [HttpGet]
        public ActionResult<IList<Account>> GetAccounts()
        {
            var accounts = context.Accounts.ToList();
            if (accounts.Count!= 0)
            {
                return Ok(accounts);
            }
            else { return NotFound(); }
        }

        // GET api/accounts/5
        [HttpGet("{name}")]
        public ActionResult<Account> GetAccount(string name)
        {
            var account = context.Accounts.Where(a=>a.Name.Equals(name)).FirstOrDefault();
            if (account!= null)
            {
                var contact=context.Contacts.Where(a=>a.Email.Equals(account.ContactEmail)).FirstOrDefault();
                account.Contact=contact;
                return Ok(account);
            }
            else { return NotFound(); }
        }

        [HttpPost]
        public ActionResult<Account> PostAccount([FromBody]Account account)
        {
            var contact=context.Contacts.Where(a=>a.Email.Equals(account.Contact.Email)).FirstOrDefault();
            if (contact!=null) account.Contact=contact;
            context.Accounts.Add(account);
            context.SaveChanges();
            return StatusCode(201,account);
        }
    }
}
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace incidents.Models
{
    public class IncidentContext: DbContext
    {
        public DbSet<Incident> Incidents {get; set;}
        public DbSet<Account> Accounts {get; set;}
        public DbSet<Contact> Contacts {get; set;}
        
        
        public IncidentContext()
            : base()
        {
               Database.EnsureCreated();
            // Database.SetInitializer<BookContext>(null);
        }  

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=IncidentDB;Trusted_Connection=True;");
        }  
    }
}
using System;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace incidents.Models
{
    public class Incident
    {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public Guid Id { get; set; }
       public string Description { get; set; }
       [Required]
       public Account Account {get; set;}
       public string AccountName {get; set;}
       public Incident() {}
    }
}
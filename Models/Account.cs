using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;
using System;
namespace incidents.Models
{
    public class Account
    {
        [Key]
        public string Name {get;set;}
        [Required]
        public Contact Contact {get;set;}
        //[ForeignKey("Contact")]
        public string ContactEmail {get;set;}
        public Account() {}
    }
}
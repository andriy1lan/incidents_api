using System.ComponentModel.DataAnnotations;
using System;
namespace incidents.Models
{
    public class Contact
    {
        public string FirstName { get; set;}
        public string LastName { get; set;}
        [Key]
        public string Email { get; set;}
        //public Account Account { get; set;}
    }
}